# Learning Resources

- All About Circuits https://www.allaboutcircuits.com/education/)
- EEVBlog (http://www.eevblog.com/)
- Lynda.com (https://www.lynda.com/), *can access for free via the GVPL
- NPTEL (http://nptel.ac.in/course.php)
- Micheal Adams C++ videos (https://www.youtube.com/user/iamcanadian1867/featured)
- cppReference (http://en.cppreference.com/w/)


# Useful tools/programs

**Programming**

- VSCode (https://code.visualstudio.com/)


- Arduino IDE (https://www.arduino.cc/en/Main/Software)

**Simulation**

- 	MATLAB (https://www.mathworks.com/products/matlab.html)


- 	LTSpice (http://www.linear.com/designtools/software/)

**Version control**

- 	Git (https://git-scm.com/)


- 	Gitlab (https://gitlab.com)

**Electronics**

- 	Eagle (https://www.autodesk.com/products/eagle/overview)


- 	KiCAD (http://kicad-pcb.org/)

**CAD**

- 	AutoDesk for students (https://www.autodesk.com/education/free-software/all)


- 	Cura (https://ultimaker.com/en/products/cura-software)


- 	TinkerCAD (https://www.tinkercad.com/)

**Documentation**

- 	Typora (https://typora.io/)


- 	ShareLaTeX (https://www.sharelatex.com/)


- 	GeoGebra (https://www.geogebra.org/)



# Components

**Suppliers**

- 	BCRobotics (http://www.bc-robotics.com/)


- 	DigiKey (https://www.digikey.com/)


-	SparkFun (https://www.sparkfun.com/)


-	ABRA (https://abra-electronics.com/)


-	Adafruit (https://www.adafruit.com/)

-   MISUMI (https://us.misumi-ec.com/)



**PCB manufacturers** 

-	OSH Park (https://oshpark.com/)


-	Fritzing (https://aisler.net/fritzing)